package com.albert.Service;

import com.albert.service.IHelloService;
import com.alibaba.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Service
@Component
public class HelloServiceImpl implements IHelloService {
public String sayHello(String name){
return "Hello " + name;

    }
}
