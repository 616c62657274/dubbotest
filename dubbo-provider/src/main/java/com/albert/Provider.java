package com.albert;

import com.albert.service.IHelloService;
import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.CountDownLatch;

@EnableDubboConfiguration
@SpringBootApplication
public class Provider {
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Provider.class,args);

        new CountDownLatch(1).await();
    }
}
