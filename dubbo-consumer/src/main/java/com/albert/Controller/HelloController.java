package com.albert.Controller;

import com.albert.service.IHelloService;
import com.alibaba.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {
@Reference
private
IHelloService iHelloService;

@RequestMapping
    public String hello (@RequestParam String name){
    return iHelloService.sayHello(name);
}
}
