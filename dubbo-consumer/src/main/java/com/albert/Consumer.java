package com.albert;

import com.alibaba.dubbo.spring.boot.annotation.EnableDubboConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.concurrent.CountDownLatch;

@SpringBootApplication
@EnableDubboConfiguration
public class Consumer {
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Consumer.class,args);
        new CountDownLatch(1).await();
    }
}
