package com.albert.service;

public interface IHelloService {
    String sayHello(String name);
}
